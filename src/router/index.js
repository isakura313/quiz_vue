import { createRouter, createWebHistory } from "vue-router";
import Home from "../pages/Home.vue";
import Quiz from "../pages/Quiz.vue";

const routes = [
  { path: "/", component: Home, name: "home" },
  { path: "/quiz/:id", component: Quiz, name: "quiz" },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
